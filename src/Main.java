import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Main {

    // Liste des données (id de la donnée, objet donnée)
    public static HashMap<Integer, Iris> irisList = new HashMap<Integer, Iris>();

    // Base de données d'entrainement
    public static HashMap<Integer, Iris> trainingIrisList = new HashMap<Integer, Iris>();

    // Base de données de test
    public static HashMap<Integer, Iris> testIrisList = new HashMap<Integer, Iris>();

    // Résultats des tests
    public static List<IrisResult> irisResults = new ArrayList<IrisResult>();

    // Valeur de K dans l'algorithme KNN
    public static int K = 5;

    // Taille de la base de test
    public static int TestDatabaseSize = 9;

    // Mode d'affichage
    public static Mode mode = Mode.DATA;

    public static DistanceMode distanceMode = DistanceMode.EUCLIDIAN;

    // Mode d'affichage (récupération de données ou affichage normal)
    public enum Mode {
            NORMAL,
            DATA
    };

    public enum DistanceMode {
        EUCLIDIAN,
        MANHATTAN
    };

    // caractéristiques utilisées pour le test
    public static boolean caracteristics_PetalWidth = true;
    public static boolean caracteristics_PetalLength = true;
    public static boolean caracteristics_SetalWidth = true;
    public static boolean caracteristics_SetalLength = true;

    public static void main(String[] args) {
        readFile("iris.data");
        for(K = 5; K < 6; K++) {
            runTest(147, K, distanceMode);
            runTest(130, K, distanceMode);
            runTest(120, K, distanceMode);
            runTest(110, K, distanceMode);
            runTest(100, K, distanceMode);
            runTest(90, K, distanceMode);
            runTest(80, K, distanceMode);
            runTest(70, K, distanceMode);
            runTest(60, K, distanceMode);
            runTest(50, K, distanceMode);
            runTest(40, K, distanceMode);
            runTest(30, K, distanceMode);
            runTest(9, K, distanceMode);
            runTest(3, K, distanceMode);
        }
        /*for(K = 1; K < 40; K++) {
            runTest(70, K, distanceMode);
        }*/
    }

    /**
     * Exécute un test avec une taille de base de donnée ainsi que le K (de KNN) associé
     * @param testDatabaseSize
     * @param k
     * @param distanceMode
     */
    public static void runTest(int testDatabaseSize, int k, DistanceMode distanceMode) {
        buildDataBase(testDatabaseSize);
        if(mode.equals(Mode.NORMAL)) {
            System.out.println("Notre base de données d'apprentissage est composée de " + trainingIrisList.size() + " élements");
            System.out.println("Notre base de données de tests est composée de " + testIrisList.size() + " élements");
            System.out.println("Calcul de toutes les distances ... ");
            System.out.println("Avec K = " + k);
        }
        computeAllDistances(distanceMode);
        extractResults(k);
        int accuracy = getStatistics();
        if(mode.equals(Mode.NORMAL)) {
            System.out.println("précision du système = "+accuracy +"%");
            System.out.println("\n=========================\n");
        } else if (mode.equals(Mode.DATA)) {
            System.out.println(150-testDatabaseSize+"\t"+accuracy);
        }
    }

    /**
     * Lecture et parsing du fichier contenant les échantillons d'apprentissage et de test
     * @param fileName
     */
    public static void readFile(String fileName) {
        Iris iris;
        int id = 0;
        try {
            List<String> lines = Files.readAllLines(Paths.get(fileName), Charset.defaultCharset());
            for (String line : lines) {
                if(line != null && !line.equals("")) {
                    String[] words = line.split(",");
                    iris = new Iris(id, Float.parseFloat(words[0]), Float.parseFloat(words[1]),
                            Float.parseFloat(words[2]), Float.parseFloat(words[3]), IrisClassEnum.get(words[4]));
                    irisList.put(id, iris);
                    id++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Calcul d'une distance classique entre deux Iris selon le mode choisi
     * @param query
     * @param entry
     * @param irisResult
     */
    public static void computeDistance(Iris query, Iris entry, IrisResult irisResult, DistanceMode distanceMode) {
        float distance = 0;
        TreeMap<Integer, Float> result = irisResult.results;
        if(distanceMode.equals(DistanceMode.EUCLIDIAN)) {
            if(caracteristics_PetalLength) {
                distance += Math.pow(query.petalLength - entry.petalLength, 2);
            } if(caracteristics_PetalWidth) {
                distance += Math.pow(query.petalWidth - entry.petalWidth, 2);
            } if(caracteristics_SetalWidth) {
                distance += Math.pow(query.sepalWidth - entry.sepalWidth, 2);
            } if(caracteristics_SetalLength) {
                distance += Math.pow(query.sepalLength - entry.sepalLength, 2);
            }
            distance = (float)Math.sqrt(distance);
        } else if (distanceMode.equals(DistanceMode.MANHATTAN)) {
            if(caracteristics_PetalLength) {
                distance += Math.abs(query.petalLength - entry.petalLength);
            } if(caracteristics_PetalWidth) {
                distance += Math.abs(query.petalWidth - entry.petalWidth);
            } if(caracteristics_SetalWidth) {
                distance += Math.abs(query.sepalWidth - entry.sepalWidth);
            } if(caracteristics_SetalLength) {
                distance += Math.abs(query.sepalLength - entry.sepalLength);
            }
        }
        irisResult.results.put(entry.id, distance);
    }

    /**
     * Construction de la base de données d'apprentissage
     * @param bound
     */
    public static void buildDataBase(int bound){

        int index = bound / 3;

        //Ajout des iris du premier type pour test
        for (int i = 0;  i<index; i++){
            testIrisList.put(i, irisList.get(i));
        }
        //Ajout des iris du second type pour test
        for (int i = 50;  i<50+index; i++){
            testIrisList.put(i, irisList.get(i));
        }
        //Ajout des iris du troisième type pour test
        for (int i = 100;  i<100+index; i++){
            testIrisList.put(i, irisList.get(i));
        }

        //Construction de la base de données de test
        //Ajout des iris du premier type pour entrainement
        for (int i = index;  i<50; i++){
            trainingIrisList.put(i, irisList.get(i));
        }
        //Ajout des iris du second type pour entrainement
        for (int i = 50+index;  i<100; i++){
            trainingIrisList.put(i, irisList.get(i));
        }
        //Ajout des iris du troisième type pour entrainement
        for (int i = 100+index;  i<150; i++){
            trainingIrisList.put(i, irisList.get(i));
        }
    }

    /**
     * Calcule l'ensemble des distances entre les iris de l'échantillon de test et ceux de celui d'apprentissage
     * @param distanceMode
     */
    public static void computeAllDistances(DistanceMode distanceMode){
        Set testKeys = testIrisList.keySet();
        Set trainingKeys = trainingIrisList.keySet();
        Iterator testIt = testKeys.iterator();

        //Parcours de l'échantillon de tests
        while (testIt.hasNext()){
            Object testKey = testIt.next();
            Iris testValue= testIrisList.get(testKey);
            Iterator trainingIt = trainingKeys.iterator();

            IrisResult irisResult = new IrisResult(testValue.id, new TreeMap<Integer, Float>());

            //Parcours de l'échantillon d'entrainement
            while (trainingIt.hasNext()){
                Object trainingKey = trainingIt.next();
                Iris trainingValue = trainingIrisList.get(trainingKey);
                computeDistance(testValue, trainingValue, irisResult, distanceMode);
            }
            irisResult.sortResults();
            irisResults.add(irisResult);
        }
    }

    /**
     * Extraction des résultats en analysant les K premiers voisins
     * @param k
     */
    public static void extractResults(int k) {
        for(IrisResult irisResult : irisResults) {
            int setosaClassCount = 0;
            int versicolourClassCount = 0;
            int virginicaClassCount = 0;
            int getNeighbours = 0;

            // Récupération des K premiers voisins
            for (Map.Entry<Integer, Float> entry : irisResult.results.entrySet()) {
                if(getNeighbours >= k){
                    break;
                }
                if (irisList.get(entry.getKey()).irisClass.equals(IrisClassEnum.Setosa)) {
                    setosaClassCount++;
                } else if (irisList.get(entry.getKey()).irisClass.equals(IrisClassEnum.Versicolour)) {
                    versicolourClassCount++;
                } else if (irisList.get(entry.getKey()).irisClass.equals(IrisClassEnum.Virginica)) {
                    virginicaClassCount++;
                }
                getNeighbours++;
            }

            // détermination de la classe la plus fréquemment observée
            if(setosaClassCount > versicolourClassCount &&  setosaClassCount > virginicaClassCount) {
                irisResult.irisClass = IrisClassEnum.Setosa;
            } else if (versicolourClassCount > setosaClassCount &&  versicolourClassCount > virginicaClassCount) {
                irisResult.irisClass = IrisClassEnum.Versicolour;
            } else if (virginicaClassCount > setosaClassCount &&  virginicaClassCount > versicolourClassCount) {
                irisResult.irisClass = IrisClassEnum.Virginica;
            } else {
                irisResult.irisClass = IrisClassEnum.None;
            }
        }
    }

    /**
     * Calcul et affichage des statistiques de notre système
     * @return
     */
    public static int getStatistics() {
        int accuracy = 0;
        int itemCount = 0;
        int itemOK = 0;
        int itemKO = 0;
        for(IrisResult irisResult : irisResults) {
            if(irisResult.irisClass.equals(irisList.get(irisResult.id).irisClass)) {
                itemOK++;
            } else {
                itemKO++;
            }
            itemCount++;
        }
        accuracy = Math.round(((float)itemOK / (float)itemCount) * 100);
        return accuracy;
    }
}