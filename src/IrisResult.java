import java.util.*;

/**
 * Classe qui représente un test (d'une entrée de test contre toutes les entrées d'entrainement)
 * Elle stock les résultats de distance et contiendra la classe déterminée par le système
 */
public class IrisResult {
    // id de la donnée testée
    int id = -1;

    // liste des distances de la donnée actuelle et les autres données d'entrainement
    TreeMap<Integer, Float> results = new TreeMap<Integer, Float>();
    IrisClassEnum irisClass;

    public IrisResult(int id, TreeMap<Integer, Float> results) {
        this.id = id;
        this.results = results;
    }

    /**
     * Trie les résultats selon l'ordre croissant
     */
    public void sortResults() {
        ValueComparator vc =  new ValueComparator(results);
        TreeMap<Integer,Float> sortedMap = new TreeMap<Integer,Float>(vc);
        sortedMap.putAll(results);
        results = sortedMap;
    }
}


