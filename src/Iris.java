/**
 * Classe Qui représente une donnée (une fleur) et ses caractéristiques
 */
public class Iris {
    int id = -1;
    float sepalLength;
    float sepalWidth;
    float petalLength;
    float petalWidth;
    IrisClassEnum irisClass;

    public Iris(int id, float sepalLength, float sepalWidth, float petalLength, float petalWidth, IrisClassEnum irisClass) {
        this.id = id;
        this.sepalLength = sepalLength;
        this.sepalWidth = sepalWidth;
        this.petalLength = petalLength;
        this.petalWidth = petalWidth;
        this.irisClass = irisClass;
    }
}
