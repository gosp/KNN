/**
 * Enum qui représente les classes des données
 */
public enum IrisClassEnum {
    Setosa,
    Versicolour,
    Virginica,
    None;

    public static IrisClassEnum get(String value) {
        if(value.equals("Iris-setosa")) {
            return IrisClassEnum.Setosa;
        } else if(value.equals("Iris-versicolor")) {
            return IrisClassEnum.Versicolour;
        } else if(value.equals("Iris-virginica")) {
            return IrisClassEnum.Virginica;
        } else {
            return IrisClassEnum.None;
        }
    }
}
