import java.util.Comparator;
import java.util.Map;

/**
 * Comparateur utilisé pour trier un tableau dans l'odre croissant de valeurs
 */
class ValueComparator implements Comparator<Integer> {

    Map<Integer, Float> map;

    public ValueComparator(Map<Integer, Float> base) {
        this.map = base;
    }

    public int compare(Integer a, Integer b) {
        if (map.get(a) >= map.get(b)) {
            return 1;
        } else {
            return -1;
        }
    }
}